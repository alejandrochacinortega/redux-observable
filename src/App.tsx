import React, { Component } from "react";
import "./App.css";
import { connect } from "react-redux";
import Beers from "./components/Beers";

class App extends Component {
  render() {
    console.log(this.props);
    return (
      <div>
        <Beers />
      </div>
    );
  }
}

export default connect((state: any) => state.app)(App);
