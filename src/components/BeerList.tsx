//@ts-nochects-ignore

import React, { Component } from "react";

interface IBeersProps {
  beers?: string[];
}

interface IBeersState {}

class BeerList extends Component<IBeersProps, IBeersState> {
  render() {
    console.log(this.props);
    return (
      <ul className="List">
        {this.props.beers &&
          this.props.beers.map((beer: any) => {
            return (
              <li key={beer.id} className="List-item">
                <figure className="List-item-img">
                  <img src={beer.image_url} alt="beer" />
                </figure>
                <div className="List-item-info">
                  <p>{beer.name}</p>
                  <ul>
                    <li>
                      <small>ABV: {beer.abv}</small>
                    </li>
                    <li>
                      <small>
                        Volume: {beer.volume.unit} {beer.volume.unit}
                      </small>
                    </li>
                  </ul>
                </div>
              </li>
            );
          })}
      </ul>
    );
  }
}

export default BeerList;
