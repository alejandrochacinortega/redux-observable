// @ts-nocheck
import React, { Component } from "react";
import { connect } from "react-redux";
import BeerList from "./BeerList";
import { search, cancel, random } from "../ducks/beers/actions";
import { setConfig } from "../ducks/config/configActions";

interface IBeersProps {
  status?: string;
  data?: string[];
  search?: (value: string) => void;
}

interface IBeersState {}

class Beers extends Component<IBeersProps, IBeersState> {
  render() {
    const {
      data,
      status,
      search,
      cancel,
      messages,
      setConfig,
      config,
      random,
    } = this.props;

    const onCancel = () => {
      console.log("Canceling");
      this.props.cancel();
    };
    return (
      <div>
        <div className="App-inputs">
          {/* <select
            name="per-page"
            defaultValue={config.perPage}
            onChange={(e: any) =>
              setConfig({ perPage: Number(e.target.value) })
            }
          >
            {[1, 2, 3, 4, 5, 6, 7, 8, 9, 10].map((value: any) => {
              return (
                <option value={value} key={value}>
                  {value} result
                </option>
              );
            })}
          </select> */}
          <input
            type="text"
            placeholder="Search beers"
            onChange={(event: any) => search(event.target.value)}
          />

          {/* <button type="button" onClick={random}>
            Random
          </button> */}

          {status === "pending" && (
            <div>
              <button type="button" onClick={onCancel}>
                Cancel
              </button>
              <span className="App-spinner">
                <img src={"/ajax-loader.gif"} alt="loader" />
              </span>
            </div>
          )}
        </div>
        {status === "success" && (
          <div className="App-content">
            <BeerList beers={data} />
          </div>
        )}

        {status === "failure" && (
          <div className="App-content">
            <p>Oppps {messages[0].text}</p>
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    ...state.beers,
    config: state.config,
  };
};

export default connect(mapStateToProps, {
  search,
  cancel,
  setConfig,
  random,
})(Beers);
