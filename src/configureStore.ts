import { ajax } from "rxjs/ajax";
import { persistEpic, hydrate } from "./ducks/config/epic";
import { configReducer } from "./ducks/config/configReducer";
import { beersReducer } from "./ducks/beers/reducer";
import { createStore, combineReducers, applyMiddleware, compose } from "redux";
import { appReducer } from "./ducks/appReducer";
import { combineEpics, createEpicMiddleware } from "redux-observable";
import { fetchBeersEpic } from "./ducks/beers/epic";

export const configureStore = (dependencies = {}) => {
  const rootEpic = combineEpics(fetchBeersEpic, persistEpic, hydrate);

  const epicMiddleware = createEpicMiddleware({
    dependencies: {
      getJSON: ajax.getJSON,
      document: document,
      ...dependencies,
    },
  });

  const rootReducer = combineReducers({
    app: appReducer,
    beers: beersReducer,
    config: configReducer,
  });

  const composeEnhancers =
    // @ts-ignore
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

  const store = createStore(
    rootReducer,
    composeEnhancers(applyMiddleware(epicMiddleware))
  );

  epicMiddleware.run(rootEpic);

  return store;
};
