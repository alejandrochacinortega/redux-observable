import { ofType } from "redux-observable";
import { SET_CONFIG, setConfig } from "./configActions";
import { withLatestFrom, pluck, tap, ignoreElements } from "rxjs/operators";
import { of, EMPTY } from "rxjs";

const CACHE_KEY = "ro-config";

export const persistEpic = (action$: any, state$: any) => {
  return action$.pipe(
    ofType(SET_CONFIG),
    withLatestFrom(state$.pipe(pluck("config"))),
    tap(([action, config]) => {
      localStorage.setItem(CACHE_KEY, JSON.stringify(config));
    }),
    ignoreElements()
  );
};

export const hydrate = () => {
  const maybeConfig = localStorage.getItem(CACHE_KEY);
  if (typeof maybeConfig === "string") {
    try {
      const parsed = JSON.parse(maybeConfig);
      return of(setConfig(parsed));
    } catch (error) {
      return EMPTY;
    }
  }

  return EMPTY;
};
