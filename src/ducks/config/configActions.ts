export const SET_CONFIG = "SET_CONFIG";

export const setConfig = (partalObject: any) => {
  return {
    type: SET_CONFIG,
    payload: partalObject,
  };
};
