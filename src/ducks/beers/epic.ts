//@ts-nocheck
import {
  fetchFullfilled,
  SEARCH,
  setStatus,
  fetchFailed,
  CANCEL,
  reset,
  RANDOM,
} from "./actions";
import {
  map,
  switchMap,
  debounceTime,
  filter,
  catchError,
  mapTo,
  withLatestFrom,
  pluck,
} from "rxjs/operators";
import { concat, of, fromEvent, race, merge, forkJoin } from "rxjs";
import { ofType } from "redux-observable";

const API = "https://api.punkapi.com/v2/beers";
const search = (apiBase: string, perPage: string, payload: string) =>
  `${apiBase}?beer_name=${encodeURIComponent(payload)}&per_page=${perPage}`;

const random = (apiBase: string) => `${apiBase}/random`;

// export const fetchBeersEpic = (action$: any, state$: any, { getJSON, document }) => {
//   return action$.pipe(
//     ofType(RANDOM),
//     debounceTime(500),
//     withLatestFrom(state$.pipe(pluck("config"))),
//     switchMap(([{ payload }, config]) => {
//       const request = [...Array(config.perPage)].map(() => {
//         return getJSON(random(config.apiBase)).pipe(pluck(0));
//       });

//       const ajax$ = forkJoin(request).pipe(
//         map((resp) => fetchFullfilled(resp)),
//         catchError((err) => {
//           return of(fetchFailed(err.response.message));
//         })
//       );

//       const blockers$ = merge(
//         action$.pipe(ofType(CANCEL)),
//         fromEvent(document, "keyup").pipe(
//           filter((evt) => evt.key === "Escape" || evt.key === "Esc")
//         )
//       ).pipe(mapTo(reset()));

//       return concat(of(setStatus("pending")), race(ajax$, blockers$));
//     })
//   );
// };

export const fetchBeersEpic = (
  action$: any,
  state$: any,
  { getJSON, document }
) => {
  return action$.pipe(
    ofType(SEARCH),
    debounceTime(500),
    filter(({ payload }) => payload.trim() !== ""),
    withLatestFrom(
      state$.pipe(pluck("config"))
      // state$.pipe(pluck("user", "authToken")) // for example
    ),
    switchMap(([{ payload }, config]) => {
      const ajax$ = getJSON(
        search(config.apiBase, config.perPage, payload)
      ).pipe(
        map((resp) => fetchFullfilled(resp)),
        catchError((err) => {
          return of(fetchFailed(err.response.message));
        })
      );

      const blockers$ = merge(
        action$.pipe(ofType(CANCEL)),
        fromEvent(document, "keyup").pipe(
          filter((evt) => evt.key === "Escape" || evt.key === "Esc")
        )
      ).pipe(mapTo(reset()));

      return concat(of(setStatus("pending")), race(ajax$, blockers$));
    })
  );
};
