export const FETCH_FULFILLED = "FETCH_FULFILLED";
export const SET_STATUS = "SET_STATUS";
export const FETCH_DATA = "FETCH_DATA";
export const SEARCH = "SEARCH";
export const CANCEL = "CANCEL";
export const FETCH_FAILED = "FETCH_FAILED";
export const RESET = "RESET";
export const RANDOM = "RANDOM";

export const fetchFullfilled = (beers: any) => {
  return {
    type: FETCH_FULFILLED,
    payload: beers,
  };
};

export const setStatus = (status: string) => {
  return {
    type: SET_STATUS,
    payload: status,
  };
};

export const fetchData = (status: string) => {
  return {
    type: FETCH_DATA,
  };
};

export const fetchFailed = (error: any) => {
  return {
    type: FETCH_FAILED,
    payload: error,
  };
};

export const search = (input: string) => {
  return {
    type: SEARCH,
    payload: input,
  };
};

export const cancel = () => {
  return {
    type: CANCEL,
  };
};

export const reset = () => {
  return {
    type: RESET,
  };
};

export const random = () => {
  return {
    type: RANDOM,
  };
};
