import { FETCH_FULFILLED, SET_STATUS, RESET, FETCH_FAILED } from "./actions";

const initialState = {
  data: [],
  status: "idle", // idle || pending || success || failure
};

export const beersReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case SET_STATUS:
      return {
        ...state,
        status: action.payload,
      };

    case RESET:
      return {
        ...state,
        status: "idle",
        messages: [],
      };
    case FETCH_FULFILLED:
      return {
        ...state,
        data: action.payload,
        loading: false,
        status: "success",
        messages: [],
      };

    case FETCH_FAILED:
      return {
        ...state,
        data: action.payload,
        loading: false,
        status: "failure",
        messages: [
          {
            type: "error",
            text: action.payload,
          },
        ],
      };

    default:
      return state;
  }
};
