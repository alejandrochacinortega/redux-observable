export const appReducer = (state = { name: "alejandro" }, action: any) => {
  switch (action.type) {
    case "SET_NAME":
      return {
        ...state,
        name: action.payload,
      };

    default:
      return state;
  }
};
